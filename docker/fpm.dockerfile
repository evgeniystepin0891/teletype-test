FROM php:8.3-fpm
ENV TZ="Russia/Moscow"
COPY docker/php.ini /usr/local/etc/php/php.ini
WORKDIR /var/www/teletype

RUN apt-get update \
    && apt-get install -y libzip-dev zip \
    && docker-php-ext-install pdo pdo_mysql zip

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN pecl install install xdebug\
    && docker-php-ext-enable xdebug

EXPOSE 9000
ENTRYPOINT php-fpm

