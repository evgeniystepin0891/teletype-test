<?php

use yii\mutex\MysqlMutex;
use yii\queue\db\Queue;

return [
    'class' => Queue::class,
    'db' => 'db',
    'tableName' => '{{%queue}}',
    'channel' => 'default',
    'mutex' => MysqlMutex::class,
];