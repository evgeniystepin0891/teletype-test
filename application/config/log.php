<?php
return [
    'traceLevel' => YII_DEBUG ? 3 : 0,
    'flushInterval' => YII_DEBUG ? 1 : 100,
    'targets' => [
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['error', 'warning'],
        ],
        [
            'class' => 'yii\log\FileTarget',
            'categories' => ['webhook'],
            'except' => ['application'],
            'logFile' => '@runtime/logs/webhook.log'
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['info'],
            'categories' => ['teletype-message-client'],
            'except' => ['application'],
            'logFile' => '@runtime/logs/client.log',
            'logVars' => []
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['info'],
            'categories' => ['teletype-message-operator'],
            'except' => ['application'],
            'logFile' => '@runtime/logs/operator.log',
            'logVars' => []
        ],
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['info'],
            'categories' => ['teletype-message-other'],
            'except' => ['application'],
            'logFile' => '@runtime/logs/teletype-messages.log',
            'logVars' => []
        ]
    ],
];