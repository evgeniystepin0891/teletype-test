<?php
return [
    'teletypeApiBaseUrl' => 'https://api.teletype.app',
    'teletypeApiToken' => 'token',
    'teletypeHookSecret' => 'secret'
];