<?php

namespace app;

use app\contracts\PingPong\IPingPongService;
use app\services\PingPong\Log;
use app\services\PingPong\PingPongService;
use app\services\PingPong\TeletypeRemote;
use GuzzleHttp\Client;
use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{

    /**
     * @inheritDoc
     */
    public function bootstrap($app): void
    {
        Yii::$container->setSingleton(IPingPongService::class, function () {
            return new PingPongService(
                new TeletypeRemote(
                    Yii::$app->params['teletypeApiToken'],
                    new Client(['base_uri' => Yii::$app->params['teletypeApiBaseUrl']])
                ),
                new Log()
            );
        });
    }
}