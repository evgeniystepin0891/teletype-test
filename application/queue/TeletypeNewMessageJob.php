<?php

namespace app\queue;

use app\contracts\PingPong\dto\NewMessageHookDto;
use app\contracts\PingPong\exceptions\InvalidMessageException;
use app\contracts\PingPong\IPingPongService;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\queue\JobInterface;

class TeletypeNewMessageJob extends BaseObject implements JobInterface
{
    public NewMessageHookDto $newMessageHookDto;


    /**
     * @param $queue
     * @return void
     * @throws InvalidMessageException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function execute($queue): void
    {
        Yii::$container->get(IPingPongService::class)->handleNewMessage($this->newMessageHookDto);
    }
}