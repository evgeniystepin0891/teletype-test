<?php

namespace app\services\PingPong\contracts;

interface ITeletypeRemote
{
    public function sendTextMessage(string $dialogId, string $message): void;
}