<?php

namespace app\services\PingPong\contracts;

use app\contracts\PingPong\dto\NewMessageHookDto;

interface ILog
{
    public function saveMessage(NewMessageHookDto $newMessageHookDto);
}