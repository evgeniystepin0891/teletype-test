<?php

namespace app\services\PingPong;

use app\contracts\PingPong\exceptions\PingPongBaseException;
use app\services\PingPong\contracts\ITeletypeRemote;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class TeletypeRemote implements ITeletypeRemote
{
    const int HTTP_STATUS_OK = 200;

    public function __construct(
        protected string $token,
        protected Client $client,
    ){}

    /**
     * @throws GuzzleException
     * @throws PingPongBaseException
     */
    public function sendTextMessage(string $dialogId, string $message): void
    {
        $response = $this->client->request(
            'POST',
            '/public/api/v1/message/send',
            [
                'form_params' => [
                    'dialogId' => $dialogId,
                    'text' => $message
                ],
                'headers' => [
                    'X-Auth-Token' => $this->token,
                ]
            ]
        );

        $this->getNewMessageIds($response);
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws PingPongBaseException
     */
    protected function getNewMessageIds(ResponseInterface $response): array
    {
        if ($response->getStatusCode() === self::HTTP_STATUS_OK) {
            $responseContent = json_decode($response->getBody()->getContents(), true);
            if (isset($responseContent['success']) && $responseContent['success'] === true) {
                if (isset($responseContent['data']['ids']) && is_array($responseContent['data']['ids'])) {
                    return $responseContent['data']['ids'];
                }
            }
        }

        throw new PingPongBaseException('Unexpected response');
    }
}