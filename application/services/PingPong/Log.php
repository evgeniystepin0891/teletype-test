<?php

namespace app\services\PingPong;

use app\contracts\PingPong\dto\NewMessageHookDto;
use Yii;

class Log implements contracts\ILog
{
    public function saveMessage(NewMessageHookDto $newMessageHookDto): void
    {
        if ($newMessageHookDto->client && $newMessageHookDto->isItClient) {
            Yii::info($newMessageHookDto->text, 'teletype-message-client');
        } else if ($newMessageHookDto->operator) {
            Yii::info($newMessageHookDto->text, 'teletype-message-operator');
        } else {
            Yii::info($newMessageHookDto->text, 'teletype-message-other');
        }
    }
}