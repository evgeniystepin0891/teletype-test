<?php

namespace app\services\PingPong;

use app\contracts\PingPong\dto\NewMessageHookDto;
use app\contracts\PingPong\exceptions\InvalidMessageException;
use app\contracts\PingPong\IPingPongService;
use app\services\PingPong\contracts\ILog;
use app\services\PingPong\contracts\ITeletypeRemote;

class PingPongService implements IPingPongService
{
    /**
     * @param ITeletypeRemote $teletypeRemote
     * @param ILog $log
     * @param string $pingMessage
     * @param string $pongMessage
     */
    public function __construct(
        protected ITeletypeRemote $teletypeRemote,
        protected ILog $log,
        protected string $pingMessage = 'ping?',
        protected string $pongMessage = 'pong!'
    ){}

    /**
     * @inheritdoc
     */
    public function handleNewMessage(NewMessageHookDto $newMessageHookDto): void
    {
        if ($this->isMessageClientPing($newMessageHookDto)) {
            $this->teletypeRemote->sendTextMessage($newMessageHookDto->dialogId, $this->pongMessage);
        }

        $this->log->saveMessage($newMessageHookDto);
    }

    /**
     * @param NewMessageHookDto $newMessageHookDto
     * @return bool
     * @throws InvalidMessageException
     */
    protected function isMessageClientPing(NewMessageHookDto $newMessageHookDto): bool
    {
        if ($newMessageHookDto->isItClient) {
            if (!$newMessageHookDto->client) {
                throw new InvalidMessageException('Property client must be set');
            }

            return str_contains($newMessageHookDto->text, $this->pingMessage);
        }

        return false;
    }
}