<?php

namespace app\contracts\PingPong;

use app\contracts\PingPong\dto\NewMessageHookDto;
use app\contracts\PingPong\exceptions\InvalidMessageException;

interface IPingPongService
{
    /**
     * @param NewMessageHookDto $newMessageHookDto
     * @throws InvalidMessageException
     * @return void
     */
    public function handleNewMessage(NewMessageHookDto $newMessageHookDto): void;
}