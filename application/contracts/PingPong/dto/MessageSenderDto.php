<?php

namespace app\contracts\PingPong\dto;

class MessageSenderDto
{
    public function __construct(
        public readonly string $id,
        public readonly string $name,
        public readonly ?string $avatarUrl,
    ){}
}