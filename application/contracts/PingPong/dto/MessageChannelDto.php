<?php

namespace app\contracts\PingPong\dto;

class MessageChannelDto
{
    function __construct(
        public readonly string $id,
        public readonly string $type,
        public readonly string $name,
    ){}
}