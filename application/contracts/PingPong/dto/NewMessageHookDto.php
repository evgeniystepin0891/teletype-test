<?php

namespace app\contracts\PingPong\dto;

use DateTimeImmutable;

class NewMessageHookDto
{
    const MESSAGE_STATUS_SEND_SUCCESS = 10;
    const MESSAGE_STATUS_SEND_ERROR = 20;
    const MESSAGE_STATUS_SEND_DELIVERED = 30;

    const MESSAGE_TYPE_FROM_CLIENT_NEW = 10;
    const MESSAGE_TYPE_FROM_CLIENT_UPDATED = 12;
    const MESSAGE_TYPE_FROM_CLIENT_DELETED = 14;
    const MESSAGE_TYPE_FROM_CLIENT_RESTORED = 16;
    const MESSAGE_TYPE_NOTE = 20;
    const MESSAGE_TYPE_SYSTEM = 30;

    public function __construct(
        public readonly string $id,
        public readonly string $dialogId,
        public readonly string $text,
        public readonly array $attachments,
        public readonly ?MessageSenderDto $operator,
        public readonly ?MessageSenderDto $client,
        public readonly MessageChannelDto $channel,
        public readonly int $status,
        public readonly int $type,
        public readonly DateTimeImmutable $dateTime,
        public readonly bool $isItClient
    ){}
}