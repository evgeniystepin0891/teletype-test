<?php

namespace app\requests;

use app\contracts\PingPong\dto\MessageChannelDto;
use app\contracts\PingPong\dto\MessageSenderDto;
use app\contracts\PingPong\dto\NewMessageHookDto;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use yii\base\Model;

/**
 * @property string $id
 * @property string $dialogId
 * @property string $text
 * @property array $attachments
 * @property array $operator
 * @property array $client
 * @property array $channel
 * @property int $status
 * @property int $type
 * @property array $createdAt
 */
class TeletypeNewMessageHookRequest extends Model
{
    public $id;
    public $dialogId;
    public $text;
    public $attachments;
    public $operator;
    public $client;
    public $channel;
    public $status;
    public $type;
    public $createdAt;
    public $isItClient;

    public function rules()
    {
        return [
            [['id', 'dialogId', 'text', 'channel', 'status', 'type', 'createdAt', 'isItClient'], 'required'],
            [['id', 'dialogId', 'text'], 'string'],
            ['attachments', 'each', 'rule' => ['string']],
            ['isItClient', 'boolean'],
            [['operator', 'client'], 'safe'],
        ];
    }

    /**
     * @throws Exception
     */
    public function getDto(): NewMessageHookDto
    {
        if (!$this->validate()) {
            throw new Exception('No valid data');
        }

        return new NewMessageHookDto(
            $this->id,
            $this->dialogId,
            $this->text,
            $this->attachments,
            $this->operator ? $this->buildMessageSender($this->operator) : null,
            $this->client ? $this->buildMessageSender($this->client) : null,
            $this->buildMessageChannel($this->channel),
            $this->status,
            $this->type,
            $this->buildDataTime($this->createdAt),
            $this->isItClient
        );
    }

    protected function buildMessageSender(array $data): MessageSenderDto
    {
        return new MessageSenderDto($data['id'], $data['name'], $data['avatar']);
    }

    public function buildMessageChannel(array $data): MessageChannelDto
    {
        return new MessageChannelDto($data['id'], $data['type'], $data['name']);
    }

    /**
     * @throws Exception
     */
    public function buildDataTime(array $data): DateTimeImmutable
    {
        return new DateTimeImmutable($data['date'], new DateTimeZone($data['timezone']));
    }
}