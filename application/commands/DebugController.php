<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace app\commands;

use app\contracts\PingPong\dto\MessageChannelDto;
use app\contracts\PingPong\dto\NewMessageHookDto;
use app\contracts\PingPong\exceptions\InvalidMessageException;
use app\queue\TeletypeNewMessageJob;
use app\services\PingPong\Log;
use app\services\PingPong\PingPongService;
use app\services\PingPong\TeletypeRemote;
use DateTimeImmutable;
use GuzzleHttp\Client;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DebugController extends Controller
{
    /**
     * @throws InvalidMessageException
     */
    public function actionIndex(): int
    {
        $remote = new TeletypeRemote(
            '2wOt4PEesGdlnqoJnCFqIMLpqGpk3qJ7tFe5TmAaIhNx_oD__xmUh7wsyz4GvGOG',
            new Client(['base_uri' => 'https://api.teletype.app/public/api/v1'])
        );

        $remote->sendTextMessage('Ncm4NTUYQm3MACNksABRcoIXlGTznBgTqO9nvAkEguTLjbaqaNgRQSkr4Nd3KHtl', 'test message from api');

        /*
        $pingPongService = new PingPongService(
            new TeletypeRemote(),
            new Log()
        );

        $dto = new NewMessageHookDto(
            1,
            'hello ping?',
            [],
            null,
            null,
            new MessageChannelDto(10, 'type', 'testChannel'),
            NewMessageHookDto::MESSAGE_STATUS_SEND_SUCCESS,
            NewMessageHookDto::MESSAGE_TYPE_FROM_CLIENT_NEW,
            new DateTimeImmutable()
        );

        //$pingPongService->handleNewMessage();

        Yii::$app->queue->push(
            new TeletypeNewMessageJob(['newMessageHookDto' => $dto])
        );
        */
        return ExitCode::OK;
    }
}
