<?php

namespace app\controllers;

use app\queue\TeletypeNewMessageJob;
use app\requests\TeletypeNewMessageHookRequest;
use Exception;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TeletypeController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException|InvalidConfigException
     */
    public function beforeAction($action): bool
    {
        Yii::debug(
            json_encode(Yii::$app->getRequest()->getBodyParams()),
            'webhook'
        );

        $this->response = Response::FORMAT_JSON;
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $secret
     * @return void
     * @throws ForbiddenHttpException
     */
    public function actionIndex($secret): void
    {
        if ($secret != Yii::$app->params['teletypeHookSecret']) {
            throw new ForbiddenHttpException();
        }

        $requestData = Yii::$app->request->post();
        $payload = json_decode($requestData['payload'], true);
        if (isset($requestData['name'])) {
            if ($requestData['name'] === 'new message' || $requestData['name'] === 'success send') {
                $this->handleNewMessage($payload['message']);
            }
        }

    }

    /**
     * @throws Exception
     */
    protected function handleNewMessage(array $payload): void
    {
        $request = new TeletypeNewMessageHookRequest();
        $request->load($payload, '');

        Yii::$app->queue->push(
            new TeletypeNewMessageJob(['newMessageHookDto' => $request->getDto()])
        );
    }
}